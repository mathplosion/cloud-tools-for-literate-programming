---
title: "Using Cloud Tools for Literate Programming to Redesign an AI Course"
author: 
- Calvin Williamson, Maria Hwang
date: Fashion Institute of Technology - SUNY
output:
  revealjs::revealjs_presentation:
    theme: beige 
    transition: slide
    center: true
---
# Introductry Machine Learning Course

- Business majors
- Very motivated to learn about data science  
- No programming backgrounds  

# Need an easy to manage environment

- Command line is cumbersome
- RStudio is too much

# Can we use Google Colab?

- School uses Google Docs and Gmail already
- Notebooks are just stored in Google Drive
- Its got corgis and kitties!
![](assets/Screenshot.png)

# But Colab is Python, right? 

- It can work with R !!!  
- Instructors use RMarkdown and GitHub
- Students use Colab and Google Drive

# Step 1 - RMarkdown To GitHub 

![](assets/renderprocess.svg)

- Instructor converts the RMarkdown to Ipynb 
- Instructor uploads the ipynb  to GitHub 

# Step 2 - GitHub To Colab

![](assets/GitHubToColab.svg)

- Instructor gives link to student in class 
- Student clicks and it opens in Google Colab

# Step 3 - Colab To Drive

![](assets/ColabToGoogleDrive.svg)

- Student starts to work on Notebook in Colab 
- Makes a copy and stores it Google Drive 

# Thank You 

- See you at the Posters!
- Thanks to EAAI-20 
