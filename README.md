# Cloud Tools For Literate Programming

This is the repo for building the accompanying EAAI-2020 poster for [_Using Cloud Tools for Literate Programming to Redesign an AI Course for Non-Traditional College Students_](https://gitlab.com/calvinw/cloud-tools-for-literate-programming/blob/master/eaai2020/EAAI_2020_CloudToolsPaper.pdf) This paper is in the eaai2020 directory of the repo.

The pages site for this repo hosts the html version of the poster:

[https://calvinw.gitlab.io/cloud-tools-for-literate-programming/index.html](https://calvinw.gitlab.io/cloud-tools-for-literate-programming/index.html)

The pdf version of the poster is here:

[https://calvinw.gitlab.io/cloud-tools-for-literate-programming/CloudToolsPoster.pdf](https://calvinw.gitlab.io/cloud-tools-for-literate-programming/CloudToolsPoster.pdf)

Slides for the talk are here:
[https://calvinw.gitlab.io/cloud-tools-for-literate-programming/CloudToolsSlides.html](https://calvinw.gitlab.io/cloud-tools-for-literate-programming/CloudToolsSlides.html)


For the Poster and Slides:

Make sure you have the R packages `rmarkdown` and `remotes` and `revealjs` installed in R, then install the github version of the `posterdown` package (this uses `remotes` to install a github version of a package). The CRAN version of `posterdown` is not enough, we need the github version:

```r
>install.packages("rmarkdown")
>install.packages("remotes")
>install.packages("revealjs")
>remotes::install_github('brentthorne/posterdown')
``` 

Then you can build everything by typing this at a bash shell:

```bash
make
``` 

This will build 

- CloudToolsPoster.html  html version of poster 
- CloudToolsPoster.pdf  pdf version of the poster
- CloudToolsSlides.html html slides presentation

Sometimes the html version and pdf version of the posters differ a little so always check the pdf version for correctness.
