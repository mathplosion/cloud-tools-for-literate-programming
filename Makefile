SHELL:=/bin/bash
HTML_FILES = CloudToolsPoster.html CloudToolsSlides.html 
PDF_FILES = CloudToolsPoster.pdf 

all : poster pdf slides
	@echo All html files are now up to date

clean :
	@echo Removing html and pdfs
	rm -f CloudToolsPoster.html CloudToolsPoster.pdf CloudToolsSlides.html 
	rm -rf *_files 

poster: CloudToolsPoster.html

pdf: CloudToolsPoster.pdf

slides: CloudToolsSlides.html

CloudToolsPoster.pdf : CloudToolsPoster.Rmd
	@Rscript -e 'library(knitr); library(rmarkdown)' \
			 -e 'pagedown::chrome_print("$<")'

%.html : %.Rmd
	@Rscript -e 'library(knitr); library(rmarkdown)' \
	         -e 'opts_knit[["set"]](progress=TRUE)' \
	         -e 'render("$<")'

.PHONY: all clean
